'use strict'

const Promise = require('bluebird')

const queueName = 'tasks'

const amqpLib = require('amqplib')

const rabbitPromise = amqpLib
  .connect(process.env['AMQP_CREDENTIALS'])
  .then((conn) => {
    return conn.createChannel()
  })
  .tap((ch) => {
    return ch.assertQueue(queueName)
  });

(function consume () {
  rabbitPromise.then((ch) => {
    ch.consume(queueName, (msg) => {
      console.log(`I received a message ${msg.content.toString()}`)
      ch.ack(msg)
    })
  })
}())
