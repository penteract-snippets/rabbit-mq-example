# Rabbit MQ Local Setup With Docker And Node.

This project provides examples of setting up and interacting with RabbitMQ
on your local machine.

## Project Dependencies
- node
- docker

> Note: Ensure that you can run `docker ps` without the use of sudo/please for more information [follow this link](https://docs.docker.com/install/linux/linux-postinstall/)

## Project installation

```bash
  npm install
```

## Running the Project

Start rabbit:

```bash
npm run rabbit
```

Start a consumer listening to a queue:

```bash
npm run consume
```

Publish a message to the queue:

```bash
npm run publish
```
