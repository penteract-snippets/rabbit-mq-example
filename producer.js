'use strict'

const Promise = require('bluebird')

const queueName = 'tasks'

const amqpLib = require('amqplib')

const rabbitPromise = amqpLib
  .connect(process.env['AMQP_CREDENTIALS'])
  .then((conn) => {
    return conn.createChannel()
  })
  .tap((ch) => {
    return ch.assertQueue(queueName)
  });

(function produce () {
  rabbitPromise.then((ch) => {
    ch.sendToQueue(queueName, Buffer.from('This is excellent'))
  })
}())
